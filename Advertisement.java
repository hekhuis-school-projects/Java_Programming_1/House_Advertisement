import java.awt.image.*;
import java.net.*;
import javax.imageio.*;
import java.io.*;
import java.awt.*;
import javax.swing.*;

/**
 * Class Advertisement -
 * Creates a real estate advertisement
 * 
 * @author Kyle Hekhuis 
 * @version 1
 */
public class Advertisement extends JApplet {
    /**
     * Paint method for applet.
     * 
     * @param  g   the Graphics object for this applet
     */
    public void paint(Graphics g) {
        int x = 50;
        int y = 50;
        
        g.drawRect (x, y, 500, 300);
        
        // Write out the information for ad
        Font header1 = new Font ("Times New Roman", Font.BOLD, 17);
        g.setFont (header1);
        g.drawString ("5862 Falcon's Porch Ct SW,", x + 10, y + 25);
        g.drawString ("Allendale MI, 49401", x + 35, y + 45);
        
        g.drawString ("Price: $370,000", x + 45, y + 80);
        Font header2 = new Font ("Times New Roman", Font.BOLD, 15);
        g.setFont (header2);
        g.drawString ("Custom Built, 2,250 sq.ft.,", x + 25, y + 100);
        g.drawString ("4 Bedroom, 3 1/2 Bathroom", x + 20, y + 115);
        g.drawString ("Located on a lake!", x + 50, y + 130);
        
        g.drawString ("Appliances:", x + 15, y + 170);
        Font body = new Font ("Times New Roman", Font.PLAIN, 15);
        g.setFont (body);
        g.drawString ("Washer", x + 15, y + 185);
        g.drawString ("Dryer", x + 15, y + 200);
        g.drawString ("Microwave", x + 15, y + 215);
        g.drawString ("Refrigerator", x + 15, y + 230);
        g.drawString ("Dishwasher", x + 15, y + 245);
        
        Font contact = new Font ("Times New Roman", Font.PLAIN, 12);
        g.setFont (contact);
        g.drawString ("For more information, contact at:", x + 10, y + 280);
        g.drawString ("(616) 331-5000     hekhuisk@mail.gvsu.edu", x + 10, y + 293);
               
        // Where the x and y cordinates start for the plot survey
        int plotX = x + 240;
        int plotY = y + 155;
        
        // Draw the plot survey
        Color darkGreen = new Color (0, 102, 0);
        Color waterBlue = new Color (0, 64, 128);
        g.setColor (darkGreen);
        g.fillRect (plotX, plotY, 250, 130);
        
        // Lake
        g.setColor (waterBlue);
        g.fillRect (plotX, plotY, 20, 130);
        
        // Driveway
        g.setColor (Color.white);
        g.fillRect (plotX + 170, plotY + 70, 80, 40);
        g.fillOval (plotX + 150, plotY + 37, 35, 40);
        g.fillRect (plotX + 175, plotY + 63, 10, 10);
        g.fillRect (plotX + 160, plotY + 38, 5, 5);
        g.setColor (darkGreen);
        g.fillOval (plotX + 150, plotY + 50, 25, 23);
        
        // House
        g.setColor (Color.lightGray);
        g.fillRect (plotX + 90, plotY + 10, 60, 90);
        g.fillRect (plotX + 100, plotY + 65, 70, 50);
        g.fillRect (plotX + 140, plotY + 25, 15, 40);
        g.fillRect (plotX + 140, plotY + 30, 20, 25);
        
        // Import image of house
        BufferedImage photo = null;
        try {
            URL u = new URL (getCodeBase(), "House.jpg");
            photo = ImageIO.read(u);
        } catch (IOException e){
            g.drawString ("Problem reading the file", 100, 100);
        }
        g.drawImage (photo, x + 240, y + 10, 250, 127, null);
    }
}
