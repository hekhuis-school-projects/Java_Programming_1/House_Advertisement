Advertisement
Language: Java
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 163 - Java Programming 1
Semester / Year: Fall 2014

This program creates a simple advertisement for a house using Java graphics.

Usage:
Run 'Advertisement'.